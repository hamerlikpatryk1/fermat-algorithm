cmake_minimum_required(VERSION 3.14)
project(FermatAlghorithm)

set(CMAKE_CXX_STANDARD 14)

add_executable(FermatAlghorithm main.cpp)